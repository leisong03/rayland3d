package utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gwlab on 2016/5/3.
 */
public class EasySharePreference {
    private static SharedPreferences.Editor sp_editor = null;
    private static SharedPreferences sharedPreferences = null;
    public static SharedPreferences getPrefInstance(Context c) {
        if (sharedPreferences == null) {
            sharedPreferences = c.getSharedPreferences("3d_product", Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }
    public static SharedPreferences.Editor getEditorInstance(Context c) {
        if (sp_editor == null) {
            sp_editor = getPrefInstance(c).edit();
        }
        return sp_editor;
    }
}
