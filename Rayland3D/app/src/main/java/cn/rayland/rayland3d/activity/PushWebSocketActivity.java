package cn.rayland.rayland3d.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import cn.rayland.rayland3d.R;
import utils.EasySharePreference;

/**
 * Created by wuyaxian on 2016/5/3.
 */
public class PushWebSocketActivity extends AppCompatActivity {
    Switch pushSwitch;
    EditText address;
    EditText port;
    EditText interval;
    EditText transPort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pushwebsocket);
        initView();
        address.setText(EasySharePreference.getPrefInstance(this).getString("pushAddress", ""));
        port.setText(EasySharePreference.getPrefInstance(this).getInt("pushPort", 8001) + "");
        interval.setText((EasySharePreference.getPrefInstance(this).getInt("pushTime", 5000) / 1000) + "");
        pushSwitch.setChecked(EasySharePreference.getPrefInstance(this).getBoolean("ifpush", false));

        pushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                EasySharePreference.getEditorInstance(getApplicationContext()).putBoolean("ifpush", b).commit();
            }
        });
        transPort.setText(EasySharePreference.getPrefInstance(this).getInt("transPort", 8002) + "");
    }

    private void initView() {
        pushSwitch = (Switch) findViewById(R.id.ifpush);
        address = (EditText) findViewById(R.id.address);
        port = (EditText) findViewById(R.id.port);
        interval = (EditText) findViewById(R.id.interval);
        transPort = (EditText) findViewById(R.id.transPort);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EasySharePreference.getEditorInstance(this).putString("pushAddress", address.getText().toString().trim()).commit();
        EasySharePreference.getEditorInstance(this).putInt("pushPort", Integer.parseInt(port.getText().toString().trim())).commit();
        EasySharePreference.getEditorInstance(this).putInt("pushTime", Integer.parseInt(interval.getText().toString().trim()) * 1000).commit();
        EasySharePreference.getEditorInstance(this).putInt("transPort", Integer.parseInt(transPort.getText().toString().trim())).commit();
    }
}
