package cn.rayland.rayland3d.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import cn.rayland.library.bean.GcodeTask;
import cn.rayland.library.bean.MachineTask;
import cn.rayland.library.utils.MachineManager;
import cn.rayland.rayland3d.app.App;

/**
 * Created by gw on 2016/3/31.
 */
public class LaserRangActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = LaserRangActivity.class.getSimpleName();
    private Handler handler = new Handler();
    private Runnable runnable;
    private TextView printState;
    private GcodeTask task = new GcodeTask();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button btn1 = new Button(this);
        btn1.setText("G1 X5 Y5 Z5\nM114\nG1 X-5 Y-5 Z-5\nM114");
        Button btn2 = new Button(this);
        btn2.setText("G1 X5 Y5 Z5\nM900\nG1 X-5 Y-5 Z-5\nM900");
        Button btn3 = new Button(this);
        btn3.setText("g161 z\n" +
                "g161 x\n" +
                "g161 y\n" +
                "g92 x0 y0 z0\n" +
                "g1 x2 F1500\n" +
                "G161 x\n" +
                "g92 x0 y0 z0\n" +
                "g1 y2\n" +
                "G161 y\n" +
                "g92 x0 y0 z0\n" +
                "g1 z2\n" +
                "G161 z\n" +
                "g92 x0 y0 z0\n" +
                "M104 S45\n" +
                "M83");
        btn2.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn3.setOnClickListener(this);
        printState = new TextView(this);

        ScrollView scrollView = new ScrollView(this);
        LinearLayout layout = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT );
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(btn1, params);
        layout.addView(btn2, params);
        layout.addView(btn3, params);
        layout.addView(printState, params);
        scrollView.addView(layout, params);
        setContentView(scrollView);

        task.setGcodeCallback(new MachineTask.GcodeCallback() {
            @Override
            public void onM900(float distance) {
                //M900回调
                Log.d(TAG, "height = " + distance + "");
            }

            @Override
            public void onM114(float[] positions) {
                //M114回调
                Log.d(TAG, "坐标 = "+positions[0] + "/" + positions[1] + "/" + positions[2]);
            }
        });

        startGetPrintState();
    }


    @Override
    public void onClick(View v) {
        task.setContent(((Button) v).getText().toString());
        App.machineManager.sendTask(task, false);
    }

    //定时获取打印机状态
    private void startGetPrintState() {
        runnable = new Runnable(){
            @Override
            public void run() {
                try {
                    printState.setText(App.machineManager.getMachineState().toString());
                }catch (Exception e){}
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(runnable, 1000);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
