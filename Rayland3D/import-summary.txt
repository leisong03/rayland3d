ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .fatjar
* 3dLibrary.jar
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:23.+

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => _dLibrary\src\main\AndroidManifest.xml
* assets\ => _dLibrary\src\main\assets\
* libs\armeabi-v7a\libhelloc.so => _dLibrary\src\main\jniLibs\armeabi-v7a\libhelloc.so
* libs\armeabi\libhelloc.so => _dLibrary\src\main\jniLibs\armeabi\libhelloc.so
* libs\fastjson-1.2.7.jar => _dLibrary\libs\fastjson-1.2.7.jar
* libs\mips\libhelloc.so => _dLibrary\src\main\jniLibs\mips\libhelloc.so
* libs\x86\libhelloc.so => _dLibrary\src\main\jniLibs\x86\libhelloc.so
* res\ => _dLibrary\src\main\res\
* src\ => _dLibrary\src\main\java\

Missing Android Support Repository:
-----------------------------------
Some useful libraries, such as the Android Support Library, are
installed from a special Maven repository, which should be installed
via the SDK manager.

It looks like this library is missing from your SDK installation at:
null

To install it, open the SDK manager, and in the Extras category,
select "Android Support Repository". You may also want to install the
"Google Repository" if you want to use libraries like Google Play
Services.

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
