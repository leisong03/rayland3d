package cn.rayland.library.bean;

import cn.rayland.api.Machine;


/**
 * Created by gw on 2016/2/23.
 */
public class SeniorSetting {
    private String name;
    private boolean selected;
    private Machine setting;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selecte) {
        this.selected = selecte;
    }

    public Machine getSetting() {
        return setting;
    }

    public void setSetting(Machine setting) {
        this.setting = setting;
    }

}
