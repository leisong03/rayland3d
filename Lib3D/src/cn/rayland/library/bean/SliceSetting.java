package cn.rayland.library.bean;

public class SliceSetting {

	private String engine;
	private float layerThickness;
	private int speed;
	private int temp;
	private float retrac_length;
	private String support;
	private String adhesion;
	private float scale;
	private float offset_x;
	private float offset_y;
	private float offset_z;
	private float rotate_x;
	private float rotate_y;
	private float rotate_z;
	public String getEngine() {
		return engine;
	}
	public void setEngine(String engine) {
		this.engine = engine;
	}
	public float getLayerThickness() {
		return layerThickness;
	}
	public void setLayerThickness(float layerThickness) {
		this.layerThickness = layerThickness;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getTemp() {
		return temp;
	}
	public void setTemp(int temp) {
		this.temp = temp;
	}
	public float getRetrac_length() {
		return retrac_length;
	}
	public void setRetrac_length(float retrac_length) {
		this.retrac_length = retrac_length;
	}
	public String getSupport() {
		return support;
	}
	public void setSupport(String support) {
		this.support = support;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}
	public float getOffset_x() {
		return offset_x;
	}
	public void setOffset_x(float offset_x) {
		this.offset_x = offset_x;
	}
	public float getOffset_y() {
		return offset_y;
	}
	public void setOffset_y(float offset_y) {
		this.offset_y = offset_y;
	}
	public float getOffset_z() {
		return offset_z;
	}
	public void setOffset_z(float offset_z) {
		this.offset_z = offset_z;
	}
	public float getRotate_x() {
		return rotate_x;
	}
	public void setRotate_x(float rotate_x) {
		this.rotate_x = rotate_x;
	}
	public float getRotate_y() {
		return rotate_y;
	}
	public void setRotate_y(float rotate_y) {
		this.rotate_y = rotate_y;
	}
	public float getRotate_z() {
		return rotate_z;
	}
	public void setRotate_z(float rotate_z) {
		this.rotate_z = rotate_z;
	}
	public String getAdhesion() {
		return adhesion;
	}
	public void setAdhesion(String adhesion) {
		this.adhesion = adhesion;
	}
	public String toParams() {
		return "?engine=" + engine + "&layerThickness="
				+ layerThickness + "&speed=" + speed + "&temp=" + temp
				+ "&retrac_length=" + retrac_length + "&support=" + support
				+ "&scale=" + scale + "&offset_x=" + offset_x + "&offset_y="
				+ offset_y + "&offset_z=" + offset_z + "&rotate=" + rotate_z;
	}
}
