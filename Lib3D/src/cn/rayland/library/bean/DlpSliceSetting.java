package cn.rayland.library.bean;

public class DlpSliceSetting {

	private String screenSize;
	private float layerThickness;
	private int layerTime;
	private int bottomLayersTime;
	private int bottomLayersNum;
	private int blankingTime;
	private int buildDirection;
	private float liftDistance;
	private int liftSpeed;
	private int retractSpeed;
	private float liquidSurfaceHeight;
	private float secondLiftDistance;
}
