package cn.rayland.library.bean;

import java.io.File;

/**
 * Created by gw on 2016/1/14.
 */
public class FileTask extends MachineTask<File>{
    protected SliceSetting sliceSetting;
    
    public FileTask(){
    	
    }
    
    public FileTask(File file){
    	this.content = file;
    }
    
    public FileTask(File file, SliceSetting setting){
    	this.content = file;
    	this.sliceSetting = setting;
    }

	public SliceSetting getSliceSetting() {
		return sliceSetting;
	}

	public void setSliceSetting(SliceSetting sliceSetting) {
		this.sliceSetting = sliceSetting;
	}
	
}
