package cn.rayland.library.bean;

import java.util.LinkedHashMap;

public class MachineTask<T> {

	protected String taskName;
	protected T content;
	protected long startTime;
	protected long usedTime;
    protected long needTime;
    protected long send;
    protected long total;
    protected int progress;
    protected LinkedHashMap<Long, Long> indexs = new LinkedHashMap<Long, Long>();
	protected TaskCallback taskCallback;
	protected ConvertCallback convertCallback;
	protected GcodeCallback gcodeCallback;

	public LinkedHashMap<Long, Long> getIndexs() {
		return indexs;
	}

	public void addIndex(long index, long endNum) {
		if(indexs.size() == 512){
			indexs.remove(indexs.keySet().iterator().next());
		}
		this.indexs.put(index, endNum);
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public long getSend() {
		return send;
	}

	public void setSend(long send) {
		this.send = send;
	}

	public T getContent() {
		return content;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public void setUsedTime(long usedTime) {
		this.usedTime = usedTime;
	}
	
	public long getUsedTime() {
		return startTime == 0 ? usedTime : usedTime + System.currentTimeMillis() - startTime;
	}

	public long getNeedTime() {
		return needTime;
	}

	public void setNeedTime(long needTime) {
		this.needTime = needTime;
	}
	
	public int getProgress() {
		return (int) (100f*send/total);
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public TaskCallback getTaskCallback() {
		return taskCallback;
	}

	public void setTaskCallback(TaskCallback taskCallback) {
		this.taskCallback = taskCallback;
	}

	public ConvertCallback getConvertCallback() {
		return convertCallback;
	}

	public void setConvertCallback(ConvertCallback convertCallback) {
		this.convertCallback = convertCallback;
	}

	public GcodeCallback getGcodeCallback() {
		return gcodeCallback;
	}

	public void setGcodeCallback(GcodeCallback gcodeCallback) {
		this.gcodeCallback = gcodeCallback;
	}

	public interface TaskCallback {
		void onStart();
		void onError();
		void onCancel();
		void onFinished();
	}
	
	public interface ConvertCallback {
		void onPreConvert(String type);

		void onConvertSuccess(String type);

		void onConvertProgress(String type, int progress);

		void onConvertFailed(String type, String error);
	}
	
	public interface GcodeCallback {
		void onM900(float distance);
		void onM114(float[] positions);
	}
}
