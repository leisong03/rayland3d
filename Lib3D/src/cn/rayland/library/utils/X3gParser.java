package cn.rayland.library.utils;

import java.io.IOException;
import java.io.InputStream;

/**
 * parser x3g bytes to least executable command
 * @author gw
 *
 */
public class X3gParser {
	public static final String TAG = X3gParser.class.getSimpleName();
	 public final static short
     HOST_CMD_VERSION = 0,
     HOST_CMD_INIT      =      1,
     HOST_CMD_GET_BUFFER_SIZE  = 2,
     HOST_CMD_CLEAR_BUFFER  = 3,
     HOST_CMD_GET_POSITION  = 4,
     HOST_CMD_ABORT         = 7,
     HOST_CMD_PAUSE         = 8,
     HOST_CMD_PROBE         = 9,
     HOST_CMD_TOOL_QUERY   = 10,
     HOST_CMD_IS_FINISHED  = 11,
     HOST_CMD_READ_EEPROM  = 12,
     HOST_CMD_WRITE_EEPROM = 13,
     HOST_CMD_CAPTURE_TO_FILE = 14,
     HOST_CMD_END_CAPTURE     = 15,
     HOST_CMD_PLAYBACK_CAPTURE = 16,
     HOST_CMD_RESET     =       17,
     HOST_CMD_NEXT_FILENAME  =  18,
     HOST_CMD_GET_DBG_REG    =  19,
     HOST_CMD_GET_BUILD_NAME =  20,
     HOST_CMD_GET_POSITION_EXT =21,
     HOST_CMD_EXTENDED_STOP    =22,
     HOST_CMD_BOARD_STATUS	  =23,
     HOST_CMD_GET_BUILD_STATS  =24,
     HOST_CMD_ADVANCED_VERSION =27,
     HOST_CMD_CONFIG = 28,
     HOST_CMD_FIND_AXES_MINIMUM=131,
     HOST_CMD_FIND_AXES_MAXIMUM=132,
     HOST_CMD_DELAY            =133,
     HOST_CMD_CHANGE_TOOL      =134,
     HOST_CMD_WAIT_FOR_TOOL    =135,
     HOST_CMD_TOOL_COMMAND     =136,
     HOST_CMD_ENABLE_AXES      =137,
     HOST_CMD_WAIT_FOR_PLATFORM=141,
     HOST_CMD_QUEUE_POINT_EXT  =139,
     HOST_CMD_SET_POSITION_EXT =140,
     HOST_CMD_QUEUE_POINT_NEW  =142,
     HOST_CMD_STORE_HOME_POSITION =143,
     HOST_CMD_RECALL_HOME_POSITION=144,
     HOST_CMD_SET_POT_VALUE    =145,
     HOST_CMD_SET_RGB_LED      =146,
     HOST_CMD_SET_BEEP         =147,
     HOST_CMD_PAUSE_FOR_BUTTON =148,
     HOST_CMD_DISPLAY_MESSAGE  =149,
     HOST_CMD_SET_BUILD_PERCENT=150,
     HOST_CMD_QUEUE_SONG =151 ,
     HOST_CMD_RESET_TO_FACTORY =152,
     HOST_CMD_BUILD_START_NOTIFICATION = 153,
     HOST_CMD_BUILD_END_NOTIFICATION=154,
     HOST_CMD_QUEUE_POINT_NEW_EXT = 155,
     HOST_CMD_SET_ACCELERATION_TOGGLE=156,
     HOST_CMD_STREAM_VERSION = 157,
     HOST_CMD_PAUSE_AT_ZPOS = 158,
     HOST_TURN_LASER    =   159,
     HOST_CMD_GET_LASER_RANGE = 160,
     HOST_CMD_LASER_FIND_MINIMUM = 161,
     HOST_CMD_LASER_FIND_MAXIMUM = 162,
     HOST_CMD_START_PROJECT    =   163,
     HOST_CMD_STOP_PROJECT    =   164,
     HOST_CMD_CHANGE_SPEED_FACTOR    =   165,
     HOST_CMD_SET_SPECIFIED_IO    =   166,
     HOST_CMD_WAIT_RISE_EDGE    =   167,
     HOST_CMD_WAIT_TRAIL_EDGE    =   168,
     HOST_CMD_DEBUG_ECHO = 0x70;
	 
	public static int readCommand(InputStream is, byte[] command, int offset) {
		int length = -1;
		try {
			if(is.read(command, offset, 1)==-1){
				return length;
			}
			switch ((int)(command[offset]&0xff)) {
			case HOST_CMD_QUEUE_POINT_EXT:
				length = 33;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_QUEUE_POINT_NEW:
				length = 26;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_QUEUE_POINT_NEW_EXT:
				length = 40;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_CHANGE_TOOL:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_ENABLE_AXES:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_SET_POSITION_EXT:
				length = 29;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_DELAY:
				length = 5;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_PAUSE_FOR_BUTTON:
				length = 5;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_DISPLAY_MESSAGE:
				length = 6;
				is.read(command, offset+1, length-1);
				while(command[length+offset-1] != '\0'){
					is.read(command, length+offset, 1);
					length ++;
				}
				break;
			case HOST_CMD_FIND_AXES_MINIMUM:
			case HOST_CMD_FIND_AXES_MAXIMUM:
				length = 8;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_WAIT_FOR_TOOL:
				length = 6;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_WAIT_FOR_PLATFORM:
				length = 6;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_STORE_HOME_POSITION:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_RECALL_HOME_POSITION:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_SET_POT_VALUE:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_SET_RGB_LED:
				length = 6;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_SET_BEEP:
				length = 6;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_TOOL_COMMAND:
				length = 4;
				is.read(command, offset+1, length-1);
				int payload = command[length+offset-1];
				is.read(command, length+offset, payload);
				length += payload;
				break;
			case HOST_CMD_SET_BUILD_PERCENT:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_QUEUE_SONG:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_RESET_TO_FACTORY:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_BUILD_START_NOTIFICATION:
				length = 6;
				is.read(command, offset+1, length-1);
				while(command[offset+length-1] != '\0'){
					is.read(command, offset+length, 1);
					length ++;
				}
				break;
			case HOST_CMD_BUILD_END_NOTIFICATION:
				length = 3;
				is.read(command, offset+1, length-1);
				while(command[offset+length-1] != '\0'){
					is.read(command, offset+length, 1);
					length ++;
				}
				break;
			case HOST_CMD_SET_ACCELERATION_TOGGLE:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_STREAM_VERSION:
				length = 21;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_PAUSE_AT_ZPOS:
				length = 5;
				is.read(command, offset+1, length-1);
				break;
			case HOST_TURN_LASER:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_GET_LASER_RANGE:
				length = 1;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_LASER_FIND_MINIMUM:
				length = 10;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_LASER_FIND_MAXIMUM:
				length = 10;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_START_PROJECT:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_STOP_PROJECT:
				length = 2;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_CONFIG:
				length = 3;
				is.read(command, offset+1, length-1);
				is.read(command, offset+length, command[offset+length-1]);
				length += command[offset+length-1];
				break;
			case HOST_CMD_CHANGE_SPEED_FACTOR:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_SET_SPECIFIED_IO:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_WAIT_RISE_EDGE:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			case HOST_CMD_WAIT_TRAIL_EDGE:
				length = 3;
				is.read(command, offset+1, length-1);
				break;
			default:
				LogUtil.e(TAG,"wrong cmd: "+(int)(command[offset]&0xff));
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return length;
	}
	
}
