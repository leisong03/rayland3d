package cn.rayland.library.utils;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import cn.rayland.api.Axis;
import cn.rayland.api.Delta;
import cn.rayland.api.Extruder;
import cn.rayland.api.HeatedBed;
import cn.rayland.api.IndependentOutput;
import cn.rayland.api.Machine;
import cn.rayland.library.bean.SeniorSetting;

/**
 * Created by gw on 2015-08-11.
 */
public class JsonUtils {

	public static List<SeniorSetting> parseSeniorSettings(String jsonString) throws JSONException{
		
		JSONArray jsonArray = new JSONArray(jsonString);
		List<SeniorSetting> seniorSettings = new ArrayList<SeniorSetting>();
		int size = jsonArray.length();
		for(int i=0;i<size;i++){
			JSONObject seniorSettingJsonObject = jsonArray.getJSONObject(i);
			SeniorSetting seniorSetting = parseSeniorSetting(seniorSettingJsonObject.toString());
			seniorSettings.add(seniorSetting);
		}
		return seniorSettings;
	}
	
public static SeniorSetting parseSeniorSetting(String jsonString) throws JSONException{
		
			JSONObject seniorSettingJsonObject = new JSONObject(jsonString);
			SeniorSetting seniorSetting = new SeniorSetting();
			seniorSetting.setName(seniorSettingJsonObject.optString("name", "unknow"));
			seniorSetting.setSelected(seniorSettingJsonObject.optBoolean("selected", false));
			
			JSONObject machineJsonObject = seniorSettingJsonObject.getJSONObject("setting");
			Machine machine = new Machine();
			machine.printTemperature = machineJsonObject.optInt("printTemperature", 210);
			machine.filamentDiameter = machineJsonObject.optDouble("filamentDiameter", 1.75);
			machine.packingDensity = machineJsonObject.optDouble("packingDensity", 0.95);
			machine.nozzleDiameter = machineJsonObject.optDouble("nozzleDiameter", 0.4);
			machine.extruderCnt = machineJsonObject.optInt("extruderCnt", 1);
			machine.timeout = machineJsonObject.optInt("timeout", 20);
			machine.type = machineJsonObject.optInt("type", 9);
			machine.holdz = machineJsonObject.optInt("holdz", 1);
			machine.switch_extruder = machineJsonObject.optInt("switch_extruder", 0);
			machine.acc_curve = machineJsonObject.optInt("acc_curve", 0);
			machine.feedrate_width = machineJsonObject.optInt("feedrate_width", 2);
			machine.puls_build_time = machineJsonObject.optInt("puls_build_time", 0);
			
			JSONObject axisXJsonObject = machineJsonObject.optJSONObject("axis_x");
			Axis axisX = new Axis();
			axisX.max_feedrate = axisXJsonObject.optDouble("max_feedrate", 16000);
			axisX.home_feedrate = axisXJsonObject.optDouble("home_feedrate", 4500);
			axisX.steps_per_mm = axisXJsonObject.optDouble("steps_per_mm", 79.1);
			axisX.length = axisXJsonObject.optDouble("length", 150);
			axisX.max_acc = axisXJsonObject.optDouble("max_acc", 600);
			axisX.max_speed_change = axisXJsonObject.optDouble("max_speed_change", 50);
			axisX.cornerPos = axisXJsonObject.optDouble("cornerPos", -6500);
			axisX.endstop = axisXJsonObject.optInt("endstop", 1);
			axisX.endstopPol = axisXJsonObject.optInt("endstopPol", 1);
			axisX.dir = axisXJsonObject.optInt("dir", 1);
			axisX.driving_voltage = axisXJsonObject.optInt("driving_voltage", 60);
			machine.axis_x = axisX;
			
			JSONObject axisYJsonObject = machineJsonObject.optJSONObject("axis_y");
			Axis axisY = new Axis();
			axisY.max_feedrate = axisYJsonObject.optDouble("max_feedrate", 16000);
			axisY.home_feedrate = axisYJsonObject.optDouble("home_feedrate", 4500);
			axisY.steps_per_mm = axisYJsonObject.optDouble("steps_per_mm", 79.26);
			axisY.length = axisYJsonObject.optDouble("length", 110);
			axisY.max_acc = axisYJsonObject.optDouble("max_acc", 600);
			axisY.max_speed_change = axisYJsonObject.optDouble("max_speed_change", 50);
			axisY.cornerPos = axisYJsonObject.optDouble("cornerPos", -5000);
			axisY.endstop = axisYJsonObject.optInt("endstop", 1);
			axisY.endstopPol = axisYJsonObject.optInt("endstopPol", 1);
			axisY.dir = axisYJsonObject.optInt("dir", 0);
			axisY.driving_voltage = axisYJsonObject.optInt("driving_voltage", 60);
			machine.axis_y = axisY;
			
			JSONObject axisZJsonObject = machineJsonObject.optJSONObject("axis_z");
			Axis axisZ = new Axis();
			axisZ.max_feedrate = axisZJsonObject.optDouble("max_feedrate", 150);
			axisZ.home_feedrate = axisZJsonObject.optDouble("home_feedrate", 300);
			axisZ.steps_per_mm = axisZJsonObject.optDouble("steps_per_mm", 381.5);
			axisZ.length = axisZJsonObject.optDouble("length", 150);
			axisZ.max_acc = axisZJsonObject.optDouble("max_acc", 300);
			axisZ.max_speed_change = axisZJsonObject.optDouble("max_speed_change", 10);
			axisZ.cornerPos = axisZJsonObject.optDouble("cornerPos", 0);
			axisZ.endstop = axisZJsonObject.optInt("endstop", 1);
			axisZ.endstopPol = axisZJsonObject.optInt("endstopPol", 1);
			axisZ.dir = axisZJsonObject.optInt("dir", 1);
			axisZ.driving_voltage = axisZJsonObject.optInt("driving_voltage", 60);
			machine.axis_z = axisZ;
			
			JSONObject axisCJsonObject = machineJsonObject.optJSONObject("axis_c");
			if(axisCJsonObject != null){
				Axis axisC = new Axis();
				axisC.max_feedrate = axisCJsonObject.optDouble("max_feedrate", 150);
				axisC.home_feedrate = axisCJsonObject.optDouble("home_feedrate", 300);
				axisC.steps_per_mm = axisCJsonObject.optDouble("steps_per_mm", 381.5);
				axisC.length = axisCJsonObject.optDouble("length", 150);
				axisC.max_acc = axisCJsonObject.optDouble("max_acc", 300);
				axisC.max_speed_change = axisCJsonObject.optDouble("max_speed_change", 10);
				axisC.cornerPos = axisCJsonObject.optDouble("cornerPos", 0);
				axisC.endstop = axisCJsonObject.optInt("endstop", 1);
				axisC.endstopPol = axisCJsonObject.optInt("endstopPol", 1);
				axisC.dir = axisCJsonObject.optInt("dir", 1);
				axisC.driving_voltage = axisCJsonObject.optInt("driving_voltage", 60);
				machine.axis_c = axisC;
			}else{
				machine.axis_c = axisX;
			}
			
			JSONObject axisDJsonObject = machineJsonObject.optJSONObject("axis_d");
			if(axisDJsonObject != null){
				Axis axisD = new Axis();
				axisD.max_feedrate = axisDJsonObject.optDouble("max_feedrate", 150);
				axisD.home_feedrate = axisDJsonObject.optDouble("home_feedrate", 300);
				axisD.steps_per_mm = axisDJsonObject.optDouble("steps_per_mm", 381.5);
				axisD.length = axisDJsonObject.optDouble("length", 150);
				axisD.max_acc = axisDJsonObject.optDouble("max_acc", 300);
				axisD.max_speed_change = axisDJsonObject.optDouble("max_speed_change", 10);
				axisD.cornerPos = axisDJsonObject.optDouble("cornerPos", 0);
				axisD.endstop = axisDJsonObject.optInt("endstop", 1);
				axisD.endstopPol = axisDJsonObject.optInt("endstopPol", 1);
				axisD.dir = axisDJsonObject.optInt("dir", 1);
				axisD.driving_voltage = axisDJsonObject.optInt("driving_voltage", 60);
				machine.axis_d = axisD;
			}else{
				machine.axis_d = axisX;
			}
			
			JSONObject extruderAJsonObject = machineJsonObject.getJSONObject("extruder_a");
			Extruder extruderA = new Extruder();
			extruderA.max_feedrate = extruderAJsonObject.optDouble("max_feedrate", 1600);
			extruderA.steps_per_mm = extruderAJsonObject.optDouble("steps_per_mm", 79.275201870333662468889989185642);
			extruderA.motor_steps = extruderAJsonObject.optDouble("motor_steps", 3200);
			extruderA.max_acc = extruderAJsonObject.optDouble("max_acc", 2000);
			extruderA.max_speed_change = extruderAJsonObject.optDouble("max_speed_change", 20);
			extruderA.cornerPos = extruderAJsonObject.optDouble("cornerPos", 0);
			extruderA.dir = extruderAJsonObject.optInt("dir", 0);
			extruderA.has_heated_build_platform = extruderAJsonObject.optInt("has_heated_build_platform", 0);
			extruderA.driving_voltage = extruderAJsonObject.optInt("driving_voltage", 80);
			extruderA.p = extruderAJsonObject.optDouble("p", 7);
			extruderA.i = extruderAJsonObject.optDouble("i", 0.325);
			extruderA.d = extruderAJsonObject.optDouble("d", 36);
			extruderA.temper_control_type = extruderAJsonObject.optInt("temper_control_type", 0);
			machine.extruder_a = extruderA;
			
			JSONObject extruderBJsonObject = machineJsonObject.getJSONObject("extruder_b");
			Extruder extruderB = new Extruder();
			extruderB.max_feedrate = extruderBJsonObject.optDouble("max_feedrate", 1600);
			extruderB.steps_per_mm = extruderBJsonObject.optDouble("steps_per_mm", 79.275201870333662468889989185642);
			extruderB.motor_steps = extruderBJsonObject.optDouble("motor_steps", 3200);
			extruderB.max_acc = extruderBJsonObject.optDouble("max_acc", 2000);
			extruderB.max_speed_change = extruderBJsonObject.optDouble("max_speed_change", 20);
			extruderB.cornerPos = extruderBJsonObject.optDouble("cornerPos", 0);
			extruderB.dir = extruderBJsonObject.optInt("dir", 1);
			extruderB.has_heated_build_platform = extruderBJsonObject.optInt("has_heated_build_platform", 0);
			extruderB.driving_voltage = extruderBJsonObject.optInt("driving_voltage", 80);
			extruderB.p = extruderBJsonObject.optDouble("p", 7);
			extruderB.i = extruderBJsonObject.optDouble("i", 0.325);
			extruderB.d = extruderBJsonObject.optDouble("d", 36);
			extruderB.temper_control_type = extruderBJsonObject.optInt("temper_control_type", 0);
			machine.extruder_b = extruderB;
			
			JSONObject independentExtruderJsonObject = machineJsonObject.optJSONObject("independent_extruder");
			IndependentOutput independentExtruder = new IndependentOutput();
			independentExtruder.temper_control_type = independentExtruderJsonObject==null ? 0 : independentExtruderJsonObject.optInt("temper_control_type", 0);
			independentExtruder.temper_control_position = independentExtruderJsonObject==null ? 0 : independentExtruderJsonObject.optInt("temper_control_position", 0);
			machine.independent_output = independentExtruder;
			
			JSONObject heatedBedJsonObject = machineJsonObject.optJSONObject("heated_bed");
			HeatedBed heatedBed = new HeatedBed();
			heatedBed.temper_control_type = heatedBedJsonObject==null ? 0 : heatedBedJsonObject.optInt("temper_control_type", 0);
			machine.heated_bed = heatedBed;
			
			JSONObject deltaJsonObject = machineJsonObject.optJSONObject("delta");
			Delta delta = new Delta();
			if(deltaJsonObject!=null){
				delta.diagonal_rod = deltaJsonObject.optDouble("diagonal_rod", 0);
				delta.smooth_rod_offset = deltaJsonObject.optDouble("smooth_rod_offset", 0);
				delta.effector_offset = deltaJsonObject.optDouble("effector_offset", 0);
				delta.carriage_offset = deltaJsonObject.optDouble("carriage_offset", 0);
				delta.container_height = deltaJsonObject.optDouble("container_height", 0);
			}else{
				delta.diagonal_rod = 0;
				delta.smooth_rod_offset = 0;
				delta.effector_offset = 0;
				delta.carriage_offset = 0;
				delta.container_height = 0;
			}
			machine.delta = delta;
			
			seniorSetting.setSetting(machine);
		return seniorSetting;
	}
}
