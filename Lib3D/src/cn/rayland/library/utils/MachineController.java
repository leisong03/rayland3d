package cn.rayland.library.utils;


import cn.rayland.api.Gpx;
import android.content.Context;

public class MachineController {
	private static MachineController instance;
	private MachineManager machineManager;
	
	public static MachineController getInstance(Context context){
		if(instance == null){
			instance = new MachineController(context);
		}
		return instance;
	}
	
	private MachineController(){}
	private MachineController(Context context){
		this.machineManager = MachineManager.getInstance(context);
	}

	public void xLeft() {
		String x_left = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
						"G1 X-100 F1000";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(x_left);
	}

	public void xRight() {
		String x_right = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 X100 F1000";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(x_right);
	}

	public void yForward() {
		String y_forward = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 Y100 F1000";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(y_forward);
	}

	public void yBackForward() {
		String y_backward = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 Y-100 F1000";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(y_backward);
	}

	public void zUp() {
		String z_up = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 Z-100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(z_up);
	}

	public void zDown() {
		String z_down = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 Z100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(z_down);
	}
	
	public void cUp() {
		String c_up = "G130 C"+machineManager.machine.axis_c.driving_voltage+" D"+machineManager.machine.axis_d.driving_voltage+"\n"+
				"G92 C0 D0\nG1 C-100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(c_up);
	}

	public void cDown() {
		String c_down = "G130 C"+machineManager.machine.axis_c.driving_voltage+" D"+machineManager.machine.axis_d.driving_voltage+"\n"+
				"G92 C0 D0\nG1 C100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(c_down);
	}
	
	public void dUp() {
		String d_up = "G130 C"+machineManager.machine.axis_c.driving_voltage+" D"+machineManager.machine.axis_d.driving_voltage+"\n"+
				"G92 C0 D0\nG1 D-100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(d_up);
	}

	public void dDown() {
		String d_down = "G130 C"+machineManager.machine.axis_c.driving_voltage+" D"+machineManager.machine.axis_d.driving_voltage+"\n"+
				"G92 C0 D0\nG1 D100 F500";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(d_down);
	}
	
	public void aUp() {
		String a_up = "G130 A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 A-600 F100";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(a_up);
	}

	public void aDown() {
		String a_down = "G130 A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 A600 F100";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(a_down);
	}
	
	public void bUp() {
		String b_up = "G130 A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 B-600 F100";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(b_up);
	}

	public void bDown() {
		String b_down = "G130 A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"G1 B600 F100";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(b_down);
	}
	
	public void heatExtruderA() {
		String heat_a = "G130 A"+machineManager.machine.extruder_a.driving_voltage+"\n"+
				"M109 T0 S"+machineManager.machine.printTemperature+"\n";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(heat_a);
	}
	
	public void heatExtruderB() {
		String heat_b = "G130 B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
				"M109 T1 S"+machineManager.machine.printTemperature+"\n";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(heat_b);
	}
	
	public void inFilament() {
		String in_filament = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
							"M109 S"+machineManager.machine.printTemperature+"\n"+
							"G92 A0\nG1 A6000 F60";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(in_filament);
	}

	public void outFilament() {
		String out_filament = "G130 X"+machineManager.machine.axis_x.driving_voltage+" Y"+machineManager.machine.axis_y.driving_voltage+" Z"+machineManager.machine.axis_z.driving_voltage+" A"+machineManager.machine.extruder_a.driving_voltage+" B"+machineManager.machine.extruder_b.driving_voltage+"\n"+
								"M109 S"+machineManager.machine.printTemperature+"\n"+
								"G92 A0\nG1 A-6000 F1000";
		Gpx.gpxInit(machineManager.machine);
		X3gExecutors.execGcode(out_filament);
	}
	
	public void startAdjustHorizon(){
		String startCode = "G130 X"
				+ machineManager.machine.axis_x.driving_voltage + " Y"
				+ machineManager.machine.axis_y.driving_voltage + " Z"
				+ machineManager.machine.axis_z.driving_voltage + " A"
				+ machineManager.machine.extruder_a.driving_voltage + " B"
				+ machineManager.machine.extruder_b.driving_voltage + "\n"
				+ "G1 Z5 F1000\nG161 X F1000\n" + "G161 Y F1000\n"
				+ "G161 Z F500\n" + "G92 X"
				+ (-machineManager.machine.axis_x.length / 2) + " Y"
				+ (-machineManager.machine.axis_y.length / 2) + " Z0 A0 B0\n";
		X3gExecutors.execGcode(startCode);
	}
	
	public void leftDown() {
		String leftDown = "G1 Z5 F1000\nG1 X"
				+ (-machineManager.machine.axis_x.length / 2) + " Y"
				+ (-machineManager.machine.axis_y.length / 2) + "\nG161 Z F500";
		X3gExecutors.execGcode(leftDown);
	}

	public void rightDown() {
		String rightDown = "G1 Z5 F1000\nG1 X"
				+ (machineManager.machine.axis_x.length / 2) + " Y"
				+ (-machineManager.machine.axis_y.length / 2) + "\nG161 Z F500";
		X3gExecutors.execGcode(rightDown);
	}

	public void leftUp() {
		String leftUp = "G1 Z5 F1000\nG1 X"
				+ (-machineManager.machine.axis_x.length / 2) + " Y"
				+ (machineManager.machine.axis_y.length / 2) + "\nG161 Z F500";
		X3gExecutors.execGcode(leftUp);
	}

	public void rightUp() {
		String rightUp = "G1 Z5 F1000\nG1 X"
				+ (machineManager.machine.axis_x.length / 2) + " Y"
				+ (machineManager.machine.axis_y.length / 2) + "\nG161 Z F500";
		X3gExecutors.execGcode(rightUp);
	}

	public void center() {
		String center = "G1 Z5 F1000\nG1 X0 Y0 F2000\nG161 Z F500";
		X3gExecutors.execGcode(center);
	}

	public void stop() {
		machineManager.cancel();
	}
}
