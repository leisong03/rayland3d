package cn.rayland.library.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class ByteUtils {
	
	public static short byte2short(byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getShort(0);
	}
	
	public static float byte2float(final byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getFloat(0);
	    }
	
	public static int byte2int(final byte[] bytes) {
		ByteBuffer buffer = ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN);
		return buffer.getInt(0);
    }
	
}
