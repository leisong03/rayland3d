package cn.rayland.library.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by gw on 2016/4/14.
 */
public class PathUtils {
    public static String getMainFolder() {
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            return null;
        }
        File extStore = Environment.getExternalStorageDirectory();
        String path = extStore.getAbsolutePath() + "/pro_3d/";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }

    public static String getRemoteFolder() {
        String path = getMainFolder() + "remoteFile";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }

    public static String getMachineFolder(){
        String path = getMainFolder() + "config";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }

    public static String getSliceFolder(){
        String path = getMainFolder() + "slice";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }

    public static String getGpxFolder(){
        String path = getMainFolder() + "gpx";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }
    
    public static String getEditorFolder(){
    	 String path = getMainFolder() + "editor";
         final File file = new File(path);
         if (!file.exists())
             file.mkdirs();
         return path;
    }

    public static String getMediaFloder(){
        String path = getMainFolder() + "media";
        final File file = new File(path);
        if (!file.exists())
            file.mkdirs();
        return path;
    }
}
