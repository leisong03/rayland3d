package cn.rayland.library.utils;

import java.io.Closeable;
import java.io.IOException;

public class StreamUtil {

	public static <T extends Closeable> void close(T stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
