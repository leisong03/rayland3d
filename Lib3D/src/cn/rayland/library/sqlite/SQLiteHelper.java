package cn.rayland.library.sqlite;

import java.io.File;

import cn.rayland.library.bean.FileTask;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper{
	public static final String TABLE_NAME= "task";
	public static final String KEY_TASK_NAME= "task_name";
	public static final String KEY_FILE_PATH= "file_path";
	public static final String KEY_USED_TIME= "used_time";
	public static final String KEY_NEED_TIME= "need_time";
	public static final String KEY_SEND_LENGTH= "send_length";
	public static final String KEY_TEMPERATURE= "print_temperature";
	public static final String KEY_COORDINATE= "current_coordinate";
	public static final String KEY_VOLTAGE= "driving_voltage";
	public static final String KEY_SPEED_FACTOR= "speed_factor";
	public static final String CREATE_TABLE = "create table "+TABLE_NAME+"(id integer primary key autoincrement, " +
																KEY_TASK_NAME+" text, " +
																KEY_FILE_PATH+" text, " +
																KEY_USED_TIME+" text, " +
																KEY_NEED_TIME+" text, " +
																KEY_SEND_LENGTH+" text, " +
																KEY_TEMPERATURE+" text, " +
																KEY_COORDINATE+" text, " +
																KEY_VOLTAGE+" tex, "+
																KEY_SPEED_FACTOR+" text)";
	
	public static final String DATABASE_NAME = "TMP_TASK";
	public static final int VERSION = 2;
	
	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int j) {
		sqLiteDatabase.execSQL("drop table if exists task");
		onCreate(sqLiteDatabase);
	}
	
	public void addTask(FileTask task, String coordinate){
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TASK_NAME, task.getTaskName());
		values.put(KEY_FILE_PATH, task.getContent().getAbsolutePath());
		values.put(KEY_USED_TIME, task.getUsedTime());
		values.put(KEY_NEED_TIME, task.getNeedTime());
		values.put(KEY_SEND_LENGTH, task.getSend());
//		values.put(KEY_TEMPERATURE, task.getTargetTemper(0));
		values.put(KEY_COORDINATE, coordinate);
//		values.put(KEY_SPEED_FACTOR, task.getSpeedFactor());
//		values.put(KEY_VOLTAGE, task.getVoltage()[0]+","+task.getVoltage()[1]+","+task.getVoltage()[2]+","+task.getVoltage()[3]+","+task.getVoltage()[4]);
		db.insert(TABLE_NAME, null, values);
		db.close();
	}
	
	public FileTask getTask(){
		FileTask task = null;
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor= db.rawQuery("select * from "+TABLE_NAME, null);
		if(cursor!=null && cursor.moveToFirst()){
			task = new FileTask();
			task.setTaskName(cursor.getString(cursor.getColumnIndex(KEY_TASK_NAME)));
			task.setContent(new File(cursor.getString(cursor.getColumnIndex(KEY_FILE_PATH))));
			task.setNeedTime(Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_NEED_TIME))));
			task.setUsedTime(Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_USED_TIME))));
			task.setSend(Long.parseLong(cursor.getString(cursor.getColumnIndex(KEY_SEND_LENGTH))));
			task.setTotal(task.getContent().length());
//			task.addTargetTemper(0, Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_TEMPERATURE))));
//			task.setSpeedFactor(Integer.parseInt(cursor.getString(cursor.getColumnIndex(KEY_SPEED_FACTOR))));
			String[] voltageStrs = cursor.getString(cursor.getColumnIndex(KEY_VOLTAGE)).split(",");
			for(int i=0;i<voltageStrs.length;i++){
//				task.getVoltage()[i] = Integer.parseInt(voltageStrs[i]);
			}
		}
		db.close();
		return task;
	}
	
	
	public String[] getCoordinate(String taskName){
		String[] coordinate = new String[3];
		SQLiteDatabase db = getReadableDatabase();
		Cursor cursor = db.rawQuery("select "+KEY_COORDINATE+" from "+TABLE_NAME+" where "+KEY_TASK_NAME+"=?", new String[]{taskName});
		if(cursor!=null && cursor.moveToFirst()){
			coordinate = cursor.getString(0).split(",");
		}
		return coordinate;
	}
	
	public void deleteTask(){
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("delete from "+TABLE_NAME);
		db.close();
	}
	
}
