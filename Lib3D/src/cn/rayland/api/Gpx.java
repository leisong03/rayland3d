package cn.rayland.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.os.Build;

/**
 * Created by Lei on 2016/1/15.
 */
public class Gpx {
	public native static byte[] gpxConvertGcode(Machine myMachine,String gcode,X3gStatus status);
    public native static byte[] getInitX3g();
    public native static byte[] gpxConverPieceGcode(String gcode,X3gStatus status);
    public native static int gpxInit(Machine myMachine);
    static{
//    	String CPU_ABI = Build.CPU_ABI;
//    	try {
//    		InputStream in = Gpx.class.getResourceAsStream("/" + CPU_ABI + "/libraylandGpx.so");
//			File f = File.createTempFile("JNI-", "Temp");
//			FileOutputStream out = new FileOutputStream(f);
//		    byte [] buf = new byte[1024];
//		    int len = -1;
//		    while ((len = in.read(buf)) > 0){
//		    	out.write(buf, 0, len);
//		    }
//		    in.close();
//		    out.close();
//		    System.load(f.getAbsolutePath());
//		    f.delete();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
    	System.loadLibrary("raylandGpx");
    }   
}