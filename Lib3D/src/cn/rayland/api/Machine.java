package cn.rayland.api;

/**
 * Created by Lei on 2/4/2016.
 */
public class Machine {
    public Axis axis_x;
    public Axis axis_y;
    public Axis axis_z;
    public Axis axis_c;
    public Axis axis_d;
    public Extruder extruder_a;
    public Extruder extruder_b;
    public HeatedBed heated_bed;
    public IndependentOutput independent_output;
    public Delta delta;
    public double filamentDiameter;
    public double packingDensity;
    public double nozzleDiameter;
    public int extruderCnt;
    public int timeout;
    public int type;
    public int printTemperature;
    public int holdz;
    public int switch_extruder;
    public int acc_curve;
    public int feedrate_width;
    public int puls_build_time;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Machine other = (Machine) obj;
		if (acc_curve != other.acc_curve)
			return false;
		if (axis_c == null) {
			if (other.axis_c != null)
				return false;
		} else if (!axis_c.equals(other.axis_c))
			return false;
		if (axis_d == null) {
			if (other.axis_d != null)
				return false;
		} else if (!axis_d.equals(other.axis_d))
			return false;
		if (axis_x == null) {
			if (other.axis_x != null)
				return false;
		} else if (!axis_x.equals(other.axis_x))
			return false;
		if (axis_y == null) {
			if (other.axis_y != null)
				return false;
		} else if (!axis_y.equals(other.axis_y))
			return false;
		if (axis_z == null) {
			if (other.axis_z != null)
				return false;
		} else if (!axis_z.equals(other.axis_z))
			return false;
		if (delta == null) {
			if (other.delta != null)
				return false;
		} else if (!delta.equals(other.delta))
			return false;
		if (extruderCnt != other.extruderCnt)
			return false;
		if (extruder_a == null) {
			if (other.extruder_a != null)
				return false;
		} else if (!extruder_a.equals(other.extruder_a))
			return false;
		if (extruder_b == null) {
			if (other.extruder_b != null)
				return false;
		} else if (!extruder_b.equals(other.extruder_b))
			return false;
		if (feedrate_width != other.feedrate_width)
			return false;
		if (Double.doubleToLongBits(filamentDiameter) != Double.doubleToLongBits(other.filamentDiameter))
			return false;
		if (heated_bed == null) {
			if (other.heated_bed != null)
				return false;
		} else if (!heated_bed.equals(other.heated_bed))
			return false;
		if (holdz != other.holdz)
			return false;
		if (independent_output == null) {
			if (other.independent_output != null)
				return false;
		} else if (!independent_output.equals(other.independent_output))
			return false;
		if (Double.doubleToLongBits(nozzleDiameter) != Double.doubleToLongBits(other.nozzleDiameter))
			return false;
		if (Double.doubleToLongBits(packingDensity) != Double.doubleToLongBits(other.packingDensity))
			return false;
		if (printTemperature != other.printTemperature)
			return false;
		if (puls_build_time != other.puls_build_time)
			return false;
		if (switch_extruder != other.switch_extruder)
			return false;
		if (timeout != other.timeout)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	
	
	

    
}
