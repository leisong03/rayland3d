package cn.rayland.api;

/**
 * Created by Lei on 2/4/2016.
 */
public class Extruder {
    public double max_feedrate;
    public double steps_per_mm;
    public double motor_steps;
    public double max_acc;
    public double max_speed_change;
    public double cornerPos;
    public int dir;
    public int has_heated_build_platform;
    public int driving_voltage;
    public double p;
    public double i;
    public double d;
    public int temper_control_type;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Extruder other = (Extruder) obj;
		if (Double.doubleToLongBits(cornerPos) != Double
				.doubleToLongBits(other.cornerPos))
			return false;
		if (Double.doubleToLongBits(d) != Double.doubleToLongBits(other.d))
			return false;
		if (dir != other.dir)
			return false;
		if (driving_voltage != other.driving_voltage)
			return false;
		if (has_heated_build_platform != other.has_heated_build_platform)
			return false;
		if (Double.doubleToLongBits(i) != Double.doubleToLongBits(other.i))
			return false;
		if (Double.doubleToLongBits(max_acc) != Double
				.doubleToLongBits(other.max_acc))
			return false;
		if (Double.doubleToLongBits(max_feedrate) != Double
				.doubleToLongBits(other.max_feedrate))
			return false;
		if (Double.doubleToLongBits(max_speed_change) != Double
				.doubleToLongBits(other.max_speed_change))
			return false;
		if (Double.doubleToLongBits(motor_steps) != Double
				.doubleToLongBits(other.motor_steps))
			return false;
		if (Double.doubleToLongBits(p) != Double.doubleToLongBits(other.p))
			return false;
		if (Double.doubleToLongBits(steps_per_mm) != Double
				.doubleToLongBits(other.steps_per_mm))
			return false;
		if (temper_control_type != other.temper_control_type)
			return false;
		return true;
	}
	
    
}
