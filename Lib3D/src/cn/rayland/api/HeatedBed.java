package cn.rayland.api;

public class HeatedBed {
	public int temper_control_type;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HeatedBed other = (HeatedBed) obj;
		if (temper_control_type != other.temper_control_type)
			return false;
		return true;
	}
	
	
}
